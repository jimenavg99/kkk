package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class Inicio extends AppCompatActivity implements View.OnClickListener {
    ImageButton btnUbicacion,btndevolución, btninventario,btnconsulta, btnactualzar, btnsalir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        btnUbicacion = (ImageButton)findViewById(R.id.ubicacion);//el id mapa es el nombre del boton menú
        btndevolución= (ImageButton) findViewById(R.id.devolucion);
        btninventario= (ImageButton) findViewById(R.id.Inventario);
        btnconsulta= (ImageButton) findViewById(R.id.Consulta);
        btnactualzar= (ImageButton) findViewById(R.id.Sincronizar);
        btnsalir= (ImageButton) findViewById(R.id.salida);
        btnUbicacion.setOnClickListener(this);
        btndevolución.setOnClickListener(this);
        btninventario.setOnClickListener(this);
        btnconsulta.setOnClickListener(this);
        btnactualzar.setOnClickListener(this);
        btnsalir.setOnClickListener(this);
        btnUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Inicio.this,Ubicacion.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ubicacion:
                //Intent a=new Intent(Inicio.this,ubicacion.class);
                //startActivity(a);
                break;
            case R.id.devolucion:
                //Intent b=new Intent(Inicio.this,devolución.class);
                //startActivity(b);
                break;
            case R.id.Inventario:
                // Intent c=new Intent(Inicio.this,Inventario.class);
                // startActivity(c);
                break;
            case R.id.Consulta:
                // Intent d=new Intent(Inicio.this,Consulta.class);
                // startActivity(d);
                break;
            case R.id.Sincronizar:
                // Intent e=new Intent(Inicio.this,Sincronizar.class);
                // startActivity(e);
                break;
            case R.id.salida:
                Intent f=new Intent(Inicio.this,MainActivity.class);
                startActivity(f);
                finish();
                break;

        }
    }
}