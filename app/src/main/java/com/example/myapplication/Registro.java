package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Registro extends AppCompatActivity implements View.OnClickListener {
    EditText user, pass;
    Button continuar, regresar;
    daoUsuario dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        user=(EditText)findViewById(R.id.RegUser);
        pass=(EditText)findViewById(R.id.RegContraseña);
        continuar=(Button)findViewById(R.id.btnContinuar);
        regresar=(Button)findViewById(R.id.btnIngreso); //El equivalente a cancelar
        continuar.setOnClickListener(this);
        regresar.setOnClickListener(this);
        dao=new daoUsuario(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnContinuar:
                Usuario u=new Usuario();
                u.setUser(user.getText().toString());
                u.setPass(pass.getText().toString());

                if (!u.isNull()){
                    Toast.makeText(this,"Error: Campos Vacios", Toast.LENGTH_LONG).show();
                }else if (dao.insertarUsuario(u)){
                    Toast.makeText(this,"Registro realizado", Toast.LENGTH_LONG).show();
                    Intent i2 =new Intent(Registro.this,MainActivity.class);
                    startActivity(i2);
                    finish();
                }else{
                    Toast.makeText(this,"Usuarior registrado.", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.btnIngreso:
                Intent i =new Intent(Registro.this, MainActivity.class);
                startActivity(i);
                finish();
                break;
        }
    }
}