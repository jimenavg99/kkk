package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
EditText user, pass;
Button btnEntrar, btnRegistrar;
daoUsuario dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user=(EditText)findViewById(R.id.RegUser);
        pass=(EditText)findViewById(R.id.RegContraseña);
        btnEntrar=(Button)findViewById(R.id.btnEntrar); //Ingreso
        btnRegistrar=(Button)findViewById(R.id.btnRegistrar); //Registro
        btnEntrar.setOnClickListener(this);
        btnRegistrar.setOnClickListener(this);
        dao=new daoUsuario(this);

    }
    public void onClick(View v){
        Intent intent=new Intent(MainActivity.this,Inicio.class);
        startActivity(intent);
    }
    /*public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEntrar:
                String u=user.getText().toString();
                String p=pass.getText().toString();
                if (u.equals("") && p.equals("")){
                    Toast.makeText(this,"Error: Campos Vacios", Toast.LENGTH_LONG).show();
                }else if (dao.login(u,p)==1){
                    Usuario ux;
                    ux = dao.getUsuario(u,p);
                    Toast.makeText(this,"Datos correctos", Toast.LENGTH_LONG).show();
                    Intent i2 =new Intent(MainActivity.this,Inicio.class);
                    i2.putExtra("Id", ux.getId());
                    startActivity(i2);
                }else{
                    Toast.makeText(this,"Usuario y/o Contraseña incorrectos", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnRegistrar:
                Intent i =new Intent(MainActivity.this, Registro.class);
                startActivity(i);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + v.getId());
        }
    }*/
}