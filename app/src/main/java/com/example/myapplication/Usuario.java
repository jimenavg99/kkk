package com.example.myapplication;

public class Usuario {
    int id; //caba mencionar que el id hace referencia al correo con el se registra
    String user, pass;

    public Usuario() {
    }

    public Usuario(int id, String user, String pass) {
        this.id = id;
        this.user = user;
        this.pass = pass;
    }

    //Este metodo comprueba si los campos estan vacios
    public boolean isNull(){
        if (user.equals("")&&pass.equals("")) {
            return false;
        }else {
            return true;
        }
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "id='" + id + '\'' +
                ", pass='" + pass + '\'' +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }
}